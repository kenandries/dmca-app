<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\PrepareNoticeRequest;
use App\Notice;
use App\Provider;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;

class NoticesController extends Controller {

	/**
	 * Create new instance
	 */
	function __construct()
	{
		$this->middleware('auth');

		parent::__construct();
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$notices = $this->user->notices()->latest()->where('content_removed', 0)->get();

		return view('notices.index')->withNotices($notices);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$providers = Provider::lists('name', 'id');

		return view('notices.create', compact('providers'));
	}

	/**
	 * Ask the user to confirm DMCA that will be delivered
	 * @param PrepareNoticeRequest $request
	 *
	 * @return \Response
	 */
	public function confirm(PrepareNoticeRequest $request)
	{
		$template = $this->compileDmcaTemplate($data = $request->all());

		session()->flash('dmca', $data);

		return view('notices.confirm', compact('template'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$notice = $this->createNotice($request);

		\Mail::queue('emails.dmca', ['notice' => $notice], function($message) use ($notice) {
			$message->from($notice->getOwnerEmail())
					->to($notice->getRecipientEmail())
					->subject('DMCA Notice');
		});

		flash('your DMCA request has beend delivered');


		return redirect('notices');
	}

	public function update($noticeId, Request $request)
	{
		$isRemoved = $request->has('content_removed');

		Notice::findOrFail($noticeId)->update(['content_removed' => $isRemoved]);
	}

	/**
	 * Compile the DMCA template from the form data
	 *
	 * @param $input
	 * @return mixed
	 */
	private function compileDmcaTemplate($input)
	{
		$data = $input + [
				'name' => $this->user->name,
				'email' => $this->user->email
			];
		$template = view()->file(app_path('Http/Templates/dmca.blade.php'), $data);

		return $template;
	}

	/**
	 * Create & Persist DMCA Notice
	 *
	 * @param Request $request
	 */
	private function createNotice(Request $request)
	{
		$notice = array_merge(session()->get('dmca'), ['template' => $request->input('template')]);

		return $this->user->notices()->create($notice);
	}

}
