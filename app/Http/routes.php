<?php

/**
 * The home page
 */
Route::get('/', ['as' => 'home', 'uses' => 'PagesController@home']);

Route::get('notices/create/confirm', 'NoticesController@confirm');
Route::resource('notices', 'NoticesController');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
